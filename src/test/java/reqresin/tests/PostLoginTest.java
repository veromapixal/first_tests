package reqresin.tests;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PostLoginTest {
    @Test
    @DisplayName("Вызов метода POST /register. Регистрация нового пользователя")
    public void unsuccessPostLogin() throws IOException, URISyntaxException, InterruptedException {
        byte[] out = "{\"email\": \"peter@klaven\"}".getBytes(StandardCharsets.UTF_8);

        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI("https://reqres.in/api/login"))
                .POST(HttpRequest.BodyPublishers.ofByteArray(out))
                .build();

        HttpResponse response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.statusCode());

        StringBuilder expectedResponseBody = new StringBuilder();
        expectedResponseBody.append("{\"error\": \"Missing password\"}");
        assertEquals(expectedResponseBody, response.body(), "response_body не соответствует, тк отсутствует username");
    }
}
