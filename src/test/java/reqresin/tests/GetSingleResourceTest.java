package reqresin.tests;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class GetSingleResourceTest {
    @Test
    @DisplayName("Вызов метода GET /singleResource. Получение информации о вызываемом ресурсе")
    public void successGetSingleResource() throws IOException {
        StringBuilder result = new StringBuilder();
        URL url = new URL("https://reqres.in/api/unknown/2");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(conn.getInputStream()))) {
            for (String line; (line = reader.readLine()) != null; ) {
                result.append(line);
            }
        }
        System.out.println(result);
    }

    @Test
    @DisplayName("Проверка получаемого статус-кода в ответ на вызов метода GET /singleResource")
    public void successGetSingleResourceStatusCode() throws IOException {
        URL url = new URL("https://reqres.in/api/unknown/2");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.connect();

        System.out.println(connection.getResponseCode());
    }
}
