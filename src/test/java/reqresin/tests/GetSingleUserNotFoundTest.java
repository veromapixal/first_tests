package reqresin.tests;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class GetSingleUserNotFoundTest {


    @Test
    @DisplayName("Проверка получаемого статус-кода в ответ на вызов метода GET /singleUserNotFound")
    public void unsuccessGetSingleUserNotFoundStatusCode() throws IOException {
        URL url = new URL("https://reqres.in/api/users/23");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.connect();

        System.out.println(connection.getResponseCode());
    }
}
